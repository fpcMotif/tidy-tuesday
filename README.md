# Code from #tidytuesday analyses

These are the R Markdown documents produced during exploratory analyses of [#tidytuesday data](https://github.com/rfordatascience/tidytuesday).